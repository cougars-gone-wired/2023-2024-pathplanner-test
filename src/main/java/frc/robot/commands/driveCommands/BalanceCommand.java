package frc.robot.commands.driveCommands;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DrivetrainSubsystem;

public class BalanceCommand extends CommandBase {
    private DrivetrainSubsystem m_drivetrainSubsystem;
    private double pitch;
    private double roll;

    private float ang = 15;
    private float prop = .6f;

    public BalanceCommand(DrivetrainSubsystem drivetrainSubsystem) {
        m_drivetrainSubsystem = drivetrainSubsystem;

        addRequirements(m_drivetrainSubsystem);
    }

    @Override
    public void execute() {
        pitch = m_drivetrainSubsystem.getPitch();
        roll = m_drivetrainSubsystem.getRoll();


        SmartDashboard.putNumber("Pitch", pitch);
        SmartDashboard.putNumber("Roll", roll);

        SmartDashboard.putNumber("Mapped Pitch", (pitch/ang)*prop);
        SmartDashboard.putNumber("Mapped Roll", (roll/ang)*prop);

        if(Math.abs(pitch) > ang) pitch = Math.copySign(ang, pitch);
        if(Math.abs(roll) > ang) roll = Math.copySign(ang, roll);
        m_drivetrainSubsystem.drive(
            // new ChassisSpeeds(0,0,0)    
            new ChassisSpeeds((Math.abs((pitch/ang)*prop) > 0.1) ? (pitch/ang)*prop : 0, (Math.abs((roll/ang)*prop) > 0.1) ? (roll/ang)*prop : 0, 0)
        );
        
    }

    @Override
    public void end(boolean interrupted) {
        m_drivetrainSubsystem.drive(
            new ChassisSpeeds(0, 0, Math.PI/50)
        );
    }
}
